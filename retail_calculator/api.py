"""Retail calculator API."""
from decimal import Decimal

import fastapi
import pydantic
from fastapi import Depends, Form, Request
from fastapi.responses import HTMLResponse
from fastapi.templating import Jinja2Templates

from retail_calculator import models, settings


ROUTER_OBJ: fastapi.APIRouter = fastapi.APIRouter()
TEMPLATES = Jinja2Templates(directory="templates/")


class PriceView:
    """Price view."""

    def prepare_discount_value(self, discount: int) -> float:
        """Prepare discount value."""
        return (100 - discount) / 100

    def prepare_tax_rate(self, state: models.StateEnum) -> Decimal:
        """Prepare tax rate."""
        return Decimal(settings.STATE_TAX_RATE[state] / 100 + 1)

    def discount_by_whole_price(self, whole_price: Decimal) -> Decimal:
        """Get discount for whole price."""
        settings.DISCOUNT_MAP.sort(reverse=True)

        base_discount: float = 1.0
        price: int
        discount: int
        for price, discount in settings.DISCOUNT_MAP:
            if whole_price >= price:
                base_discount = self.prepare_discount_value(discount)
                break
        return Decimal(base_discount)

    def whole_price(self, data_settlement: models.PriceRequestModel) -> Decimal:
        """Get whole price."""
        return data_settlement.price * data_settlement.amount

    def retail_price(self, data_settlement: models.PriceRequestModel) -> models.PriceResponseModel:
        """Get retail price."""
        whole_price: Decimal = self.whole_price(data_settlement)
        discount_price: Decimal = whole_price * self.discount_by_whole_price(whole_price)
        total_price: Decimal = discount_price * self.prepare_tax_rate(data_settlement.state)

        return models.PriceResponseModel(
            state=data_settlement.state,
            price=data_settlement.price,
            amount=data_settlement.amount,
            discount_price=Decimal(discount_price).quantize(Decimal(".01")),
            total_price=Decimal(total_price).quantize(Decimal(".01")),
        )


@ROUTER_OBJ.post(
    "/retail_calculator/",
    response_class=HTMLResponse,
    status_code=fastapi.status.HTTP_200_OK,
)
async def post_retail_calculator(
    request: Request,
    amount: int = Form(..., gt=settings.MIN_AMOUNT_GOODS),
    price: pydantic.condecimal(gt=Decimal(settings.MIN_PRICE), decimal_places=2) = Form(...),  # type: ignore
    state: models.StateEnum = Form(...),
    price_view: PriceView = Depends(PriceView),
) -> TEMPLATES.TemplateResponse:  # type: ignore
    """Retail price by form."""
    total_price_response = price_view.retail_price(models.PriceRequestModel(amount=amount, price=price, state=state))
    return TEMPLATES.TemplateResponse("form.html", {**{"request": request}, **total_price_response.dict()})


@ROUTER_OBJ.get("/retail_calculator/")
def get_retail_calculator(request: Request):
    """Retail price by form."""
    return TEMPLATES.TemplateResponse("form.html", context={"request": request})


@ROUTER_OBJ.get(
    "/api/retail_calculator/",
    response_model=models.PriceResponseModel,
    status_code=fastapi.status.HTTP_200_OK,
)
async def get_retail_calculator_api(
    price_model: models.PriceRequestModel = fastapi.Body(...),
    price_view: PriceView = Depends(PriceView),
) -> models.PriceResponseModel:
    """Retail price API."""
    return price_view.retail_price(price_model)
