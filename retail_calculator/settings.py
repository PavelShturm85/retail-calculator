"""Settings for project."""
import envparse


DOC_PREFIX: str = envparse.env("DOC_PREFIX", "/doc")
API_PREFIX: str = envparse.env("API_PREFIX", "")
TAGS_METADATA: list = [
    {"name": "retail_calculator", "description": "Methods that interacts with retail calculator."},
]

UVICORN: dict = {
    "APP_PORT": envparse.env("APP_PORT", default=9991, cast=int),
    "LOG_LEVEL": envparse.env("LOG_LEVEL", "info"),
    "APP_WORKERS": envparse.env("APP_WORKERS", default=5, cast=int),
    "APP_LIMIT_MAX_REQUESTS": envparse.env("APP_LIMIT_MAX_REQUESTS", default=5000, cast=int),
    "APP_LOOP": envparse.env("APP_LOOP", "uvloop"),
}

MIN_AMOUNT_GOODS: int = envparse.env("MIN_AMOUNT_GOODS", default=0, cast=int)
MIN_PRICE: float = envparse.env("MIN_PRICE", default=0.00, cast=float)

# Discount map (min_price_for_discount, amount_discount_in_%)
DISCOUNT_MAP: list = [(50000, 15), (10000, 10), (7000, 7), (5000, 5), (1000, 3)]
STATE_TAX_RATE: dict = {
    "UT": 6.85,
    "NV": 8.00,
    "TX": 6.25,
    "AL": 4.00,
    "CA": 8.25,
}
