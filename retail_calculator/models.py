"""Models for application."""
import decimal
import enum

import pydantic

from retail_calculator import settings


class StateEnum(str, enum.Enum):
    """State enum."""

    UT: str = "UT"
    NV: str = "NV"
    TX: str = "TX"
    AL: str = "AL"
    CA: str = "CA"


class PriceRequestModel(pydantic.BaseModel):
    """Price request model."""

    amount: int = pydantic.Field(gt=settings.MIN_AMOUNT_GOODS)
    price: pydantic.condecimal(gt=decimal.Decimal(settings.MIN_PRICE), decimal_places=2)  # type: ignore
    state: StateEnum

    class Config:
        """Config class."""

        use_enum_values: bool = True


class PriceResponseModel(PriceRequestModel):
    """Price response model."""

    total_price: pydantic.condecimal(gt=decimal.Decimal(settings.MIN_PRICE), decimal_places=2)  # type: ignore
    discount_price: pydantic.condecimal(gt=decimal.Decimal(settings.MIN_PRICE), decimal_places=2)  # type: ignore
