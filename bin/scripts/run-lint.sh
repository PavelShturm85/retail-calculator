#!/usr/bin/env bash

pylint retail_calculator tests
mypy --config-file mypy.ini .
