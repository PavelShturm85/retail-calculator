"""Tests for retail calculator FORM."""
import pytest
from fastapi.testclient import TestClient
from requests import Response

from tests import helpers


FORM_RETAIL_CALCULATOR: str = "/retail_calculator/"


@pytest.mark.parametrize("_count", range(5))
def test_get_form(client: TestClient, _count) -> None:
    """Test client positive."""

    response: Response = client.get(FORM_RETAIL_CALCULATOR)

    assert response.status_code == 200


@pytest.mark.parametrize("_count", range(5))
def test_post_form(client: TestClient, _count) -> None:
    """Test post form."""
    response: Response = client.post(FORM_RETAIL_CALCULATOR, data=helpers.generate_valid_form_data())

    assert response.status_code == 200


@pytest.mark.parametrize("_count", range(5))
def test_not_valid_form_data(client: TestClient, _count) -> None:
    """Test not valid form data."""

    response: Response = client.post(FORM_RETAIL_CALCULATOR, data=helpers.generate_not_valid_form_data())

    assert response.status_code == 422


@pytest.mark.parametrize("_count", range(5))
def test_not_valid_request(client: TestClient, _count) -> None:
    """Test not request."""

    response: Response = client.post(FORM_RETAIL_CALCULATOR, data=helpers.generate_not_valid_request())

    assert response.status_code == 422
