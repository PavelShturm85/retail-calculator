"""Tests price view."""
from decimal import Decimal

import pytest

from retail_calculator import api, models, settings
from tests import helpers


def get_discount_by_whole_price(whole_price: Decimal) -> int:
    """Get discount by price."""
    base_discount: int = 0
    if whole_price >= 50000:
        base_discount = 15
    elif whole_price >= 10000:
        base_discount = 10
    elif whole_price >= 7000:
        base_discount = 7
    elif whole_price >= 5000:
        base_discount = 5
    elif whole_price >= 1000:
        base_discount = 3
    return base_discount


def get_tax_rate_by_state(state: str) -> Decimal:
    """Get tax rate by state."""
    return Decimal(settings.STATE_TAX_RATE[state] / 100 + 1)


def get_prepared_discount(discount: int) -> float:
    """Get prepared discount."""
    return (100 - discount) / 100


@pytest.mark.parametrize('_count', range(5))
def test_prepare_tax_rate(price_view: api.PriceView, _count) -> None:
    """Test prepare tax rate method."""
    tax_rate_state: models.StateEnum = helpers.generate_random_tax_rate_state()

    tax_rate: Decimal = price_view.prepare_tax_rate(tax_rate_state)

    assert tax_rate == get_tax_rate_by_state(tax_rate_state)


@pytest.mark.parametrize('_count', range(5))
def test_prepare_discount_value(price_view: api.PriceView, _count) -> None:
    """Test prepare discount value method."""
    discount: int = helpers.generate_random_discount()

    discount_value: float = price_view.prepare_discount_value(discount)

    assert discount_value == get_prepared_discount(discount)


@pytest.mark.parametrize(
    "whole_price",
    [
        "50000.00",
        "10000.00",
        "7000.00",
        "5000.00",
        "1000.00",
        "59999.99",
        "49999.99",
        "9999.99",
        "6999.99",
        "4999.99",
        "999.99",
    ],
)
def test_discount_by_whole_price(price_view: api.PriceView, whole_price: str) -> None:
    """Test discount by whole price method."""
    prepared_price: Decimal = Decimal(whole_price)
    base_discount: int = get_discount_by_whole_price(prepared_price)

    discount_by_state: Decimal = price_view.discount_by_whole_price(prepared_price)

    assert discount_by_state == Decimal((100 - base_discount) / 100)


@pytest.mark.parametrize('_count', range(5))
def test_whole_price(price_view: api.PriceView, _count) -> None:
    """Test whole price method."""
    valid_data: models.PriceRequestModel = models.PriceRequestModel(**helpers.generate_valid_form_data())

    whole_price: Decimal = price_view.whole_price(valid_data)

    assert whole_price == valid_data.price * valid_data.amount


@pytest.mark.parametrize('_count', range(5))
def test_retail_price(price_view: api.PriceView, _count) -> None:
    """Test retail price method."""
    valid_data: models.PriceRequestModel = models.PriceRequestModel(**helpers.generate_valid_form_data())
    base_discount: Decimal = Decimal(
        get_prepared_discount(get_discount_by_whole_price(valid_data.price * valid_data.amount))
    )
    discount_price: Decimal = valid_data.price * valid_data.amount * base_discount
    total_price: Decimal = discount_price * get_tax_rate_by_state(valid_data.state)

    retail_price: models.PriceResponseModel = price_view.retail_price(valid_data)

    assert retail_price.state == valid_data.state
    assert retail_price.price == valid_data.price
    assert retail_price.amount == valid_data.amount
    assert retail_price.discount_price == discount_price.quantize(Decimal('.01'))
    assert retail_price.total_price == total_price.quantize(Decimal('.01'))
