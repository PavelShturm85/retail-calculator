"""Helpers for tests."""
from decimal import Decimal

from faker import Faker

from retail_calculator import models


FAKE_OBJ: Faker = Faker("en_US")


def generate_not_valid_form_data() -> dict:
    """Generate not valid form data."""
    return {
        "amount": float(FAKE_OBJ.pydecimal()),
        "price": float(FAKE_OBJ.pydecimal()),
        "state": FAKE_OBJ.currency_code(),
    }


def generate_random_tax_rate_state() -> models.StateEnum:
    """Generate random tax rate state."""
    return FAKE_OBJ.random.choice(list(models.StateEnum.__members__.keys()))


def generate_random_discount() -> int:
    """Generate random discount."""
    return FAKE_OBJ.random.randint(1, 99)


def generate_random_price() -> Decimal:
    """Generate random price."""
    return FAKE_OBJ.pydecimal(right_digits=2, positive=True, max_value=100000)


def generate_valid_form_data() -> dict:
    """Generate valid form data."""
    return {
        "amount": FAKE_OBJ.random.randint(1, 1000),
        "price": float(generate_random_price()),
        "state": generate_random_tax_rate_state(),
    }


def generate_not_valid_request() -> dict:
    """Generate not valid request."""
    return {FAKE_OBJ.currency_code(): FAKE_OBJ.currency_name() for _ in range(FAKE_OBJ.random.randint(2, 6))}
