"""Tests for retail calculator FORM."""
import pytest
from fastapi.testclient import TestClient
from requests import Response

from tests import helpers


API_RETAIL_CALCULATOR: str = "/api/retail_calculator/"


@pytest.mark.parametrize("_count", range(5))
def test_api(client: TestClient, _count) -> None:
    """Test api."""
    response: Response = client.get(API_RETAIL_CALCULATOR, json=helpers.generate_valid_form_data())

    assert response.status_code == 200


@pytest.mark.parametrize("_count", range(5))
def test_not_valid_data(client: TestClient, _count) -> None:
    """Test not valid data."""

    response: Response = client.get(API_RETAIL_CALCULATOR, json=helpers.generate_not_valid_form_data())

    assert response.status_code == 422


@pytest.mark.parametrize("_count", range(5))
def test_not_valid_request(client: TestClient, _count) -> None:
    """Test not request."""

    response: Response = client.get(API_RETAIL_CALCULATOR, json=helpers.generate_not_valid_request())

    assert response.status_code == 422
