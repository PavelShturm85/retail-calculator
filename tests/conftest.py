"""Project-scoped test plugins."""
import fastapi
import pytest
from fastapi.testclient import TestClient

from retail_calculator import api, settings


@pytest.fixture(scope="module")
def app() -> fastapi.FastAPI:
    """App mock fixture."""
    app_mock: fastapi.FastAPI = fastapi.FastAPI()
    app_mock.include_router(api.ROUTER_OBJ, prefix=settings.API_PREFIX)
    return app_mock


@pytest.fixture
def client(app) -> TestClient:  # pylint: disable=redefined-outer-name
    """Test client."""
    return TestClient(app)


@pytest.fixture
def price_view() -> api.PriceView:
    """Price view."""
    return api.PriceView()
