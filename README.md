Retail calculator
===
This is application for retrieving retail price.<br>

## Application features:
* Retrieving retail price from unit price, item quantity, and state abbreviation.

## Quick start:
1. `pip install poetry`
1. `poetry install`
1. `poetry shell`
1. `./bin/run.sh`

## Run tests:
* `./bin/scripts/run-tests.sh`

## Run linter:
* `./bin/scripts/run-lint.sh`

## Web interface:
* `http://localhost:9991/retail_calculator/`

## API interface:
* `http://localhost:9991/api/retail_calculator/`

## API documentation:
* `http://localhost:9991/doc`
